// Hover navigation
const headerNavList = document.querySelectorAll(".header__nav-item");

headerNavList.forEach((headerNavItem) => {
    const navItem = headerNavItem.querySelector(".nav__item");
    headerNavItem.addEventListener("mouseover", () => {
        navItem.classList.add("nav__In");
    });

    headerNavItem.addEventListener("mouseout", () => {
        navItem.classList.remove("nav__In");
    });
});

// Hover sub navigation
const accountsList = document.querySelectorAll(".account__item");

accountsList.forEach((accountItem) => {
    const accountSubNav = accountItem.querySelector(".account__subNav");
    accountItem.addEventListener("mouseover", () => {
        accountSubNav.classList.add("subNav__In");
    });
    accountItem.addEventListener("mouseout", () => {
        accountSubNav.classList.remove("subNav__In");
    });
});

// Click Nav bar
const btnSideBar = document.getElementById("btnSideBar");
const btnClose = document.getElementById("btnClose");
const navBar = document.querySelector(".header__navBar");
let isActive = !navBar.classList.contains("none");

btnSideBar.addEventListener("click", () => {
    navBar.classList.remove("none");
    document.body.style.overflowY = "hidden";
    isActive = !isActive;
});

btnClose.addEventListener("click", () => {
    navBar.classList.add("none");
    document.body.style.overflowY = "visible";
    isActive = !isActive;
});

// Click Nav Account
const navAccountList = document.querySelectorAll(".nav__account-item");
const navAccountSubList = document.querySelectorAll(".nav__account-sub");
navAccountList.forEach((navAccountItem) => {
    navAccountItem.addEventListener("click", () => {
        const subMenu = navAccountItem.nextElementSibling;
        if (subMenu && subMenu.classList.contains("nav__account-sub")) {
            navAccountSubList.forEach((sub) => {
                if (sub !== subMenu) {
                    sub.classList.add("none");
                }
            });
            subMenu.classList.toggle("none");
        }
    });
});

window.addEventListener("resize", () => {
    const windowWidth = document.body.clientWidth;
    if (isActive) {
        if (windowWidth > 990) {
            navBar.classList.add("none");
        } else {
            navBar.classList.remove("none");
        }
    }
});

// Pricing
const inputCheck = document.querySelector("#switch");
const pricingAmount = document.querySelector(".price__number");

function countUp(upTo, num, countElement) {
    const delayTime = 200 / (num - upTo);
    if (upTo <= num) {
        countElement.innerHTML = upTo;
        setTimeout(() => {
            countUp(++upTo, num, countElement);
        }, delayTime);
    }
}

function countDown(downTo, num, countElement) {
    if (downTo >= num) {
        countElement.innerHTML = downTo;
        const delayTime = 200 / (downTo - num);
        setTimeout(() => {
            countDown(--downTo, num, countElement);
        }, delayTime);
    }
}

inputCheck.addEventListener("change", () => {
    if (inputCheck.checked) {
        countUp(pricingAmount.innerHTML, 49, pricingAmount);
    } else {
        countDown(49, 29, pricingAmount);
    }
});

// Scroll
const sectionsElement = document.getElementsByTagName("section");
const sections = Array.from(sectionsElement);
const countElements = document.querySelectorAll(".count");
let animationCountDone = false;

window.addEventListener("scroll", () => {
    sections.forEach((section, index) => {
        if (index != 0) {
            if (section.id === "stats") {
                const top = section.getBoundingClientRect().top;
                const bottom = section.getBoundingClientRect().bottom;
                if (top < this.window.innerHeight && bottom >= 0) {
                    if (animationCountDone === false) {
                        countElements.forEach((countElement) => {
                            countUp(0, countElement.innerHTML, countElement);
                        });
                        animationCountDone = true;
                    }
                }
            }
        }
    });
});

var Animation = function ({ offset } = { offset: 10 }) {
    var _elements;

    var windowTop = (offset * window.innerHeight) / 100;
    var windowBottom = window.innerHeight - windowTop;
    var windowLeft = 0;
    var windowRight = window.innerWidth;

    function start(element) {
        element.style.animationDelay = element.dataset.animationDelay;
        element.style.animationDuration = element.dataset.animationDuration;
        element.classList.add(element.dataset.animation);
        element.dataset.animated = "true";
    }

    function isElementOnScreen(element) {
        var elementRect = element.getBoundingClientRect();
        var elementTop =
            elementRect.top + parseInt(element.dataset.animationOffset) ||
            elementRect.top;
        var elementBottom =
            elementRect.bottom - parseInt(element.dataset.animationOffset) ||
            elementRect.bottom;
        var elementLeft = elementRect.left;
        var elementRight = elementRect.right;

        return (
            elementTop <= windowBottom &&
            elementBottom >= windowTop &&
            elementLeft <= windowRight &&
            elementRight >= windowLeft
        );
    }

    function checkElementsOnScreen(els = _elements) {
        for (var i = 0, len = els.length; i < len; i++) {
            if (els[i].dataset.animated) continue;
            isElementOnScreen(els[i]) && start(els[i]);
        }
    }

    function update() {
        _elements = document.querySelectorAll(
            "[data-animation]:not([data-animated])"
        );
        checkElementsOnScreen(_elements);
    }

    window.addEventListener("load", update, false);
    window.addEventListener("scroll", () => checkElementsOnScreen(_elements), {
        passive: true,
    });
    window.addEventListener(
        "resize",
        () => checkElementsOnScreen(_elements),
        false
    );

    return {
        start,
        isElementOnScreen,
        update,
    };
};

var options = {
    offset: 20,
};
var animation = new Animation(options);

// Typing
const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");

const textArray = ["founders.", "developers.", "designers."];
const typingDelay = 100;
const erasingDelay = 100;
const newTextDelay = 1000;
let textArrayIndex = 0;
let charIndex = 0;

function type() {
    if (charIndex < textArray[textArrayIndex].length) {
        if (!cursorSpan.classList.contains("typing"))
            cursorSpan.classList.add("typing");
        typedTextSpan.textContent +=
            textArray[textArrayIndex].charAt(charIndex);
        charIndex++;
        setTimeout(type, typingDelay);
    } else {
        cursorSpan.classList.remove("typing");
        setTimeout(erase, newTextDelay);
    }
}

function erase() {
    if (charIndex > 0) {
        if (!cursorSpan.classList.contains("typing"))
            cursorSpan.classList.add("typing");
        typedTextSpan.textContent = textArray[textArrayIndex].substring(
            0,
            charIndex - 1
        );
        charIndex--;
        setTimeout(erase, erasingDelay);
    } else {
        cursorSpan.classList.remove("typing");
        textArrayIndex++;
        if (textArrayIndex >= textArray.length) textArrayIndex = 0;
        setTimeout(type, typingDelay + 1100);
    }
}

document.addEventListener("DOMContentLoaded", function () {
    if (textArray.length) setTimeout(type, newTextDelay + 250);
});

// Slider
const slider = document.querySelector(".slider__container");
const slides = slider.querySelector(".slides");
const elements = Array.from(slides.children);

const prevButton = slider.querySelector("#prev");
const nextButton = slider.querySelector("#next");

const first = slides.firstElementChild;
const last = slides.lastElementChild;

const imageSlider = document.querySelector(".slider__list-img");
const image = [
    "./assets/img/photos/photo-1.jpg",
    "./assets/img/photos/photo-26.jpg",
];

let index = 1;
let offset = 0;
let dragStart = null;

cloneElement = (element, refElement) => {
    const clone = element.cloneNode(true);
    slides.insertBefore(clone, refElement);
    return clone;
};

disableNativeDragging = (element) => {
    element.draggable = false;
};

withTransitionSuspended = (callback) => {
    let scheduled = null;

    return () => {
        if (scheduled) {
            window.cancelAnimationFrame(scheduled);
        }

        scheduled = window.requestAnimationFrame(() => {
            slides.style.transition = "none";
            callback();

            scheduled = window.requestAnimationFrame(() => {
                slides.style.transition = "";
                scheduled = null;
            });
        });
    };
};

toggleDisabled = (disabled) => {
    prevButton.disabled = nextButton.disabled = disabled;
};

getOffset = () => {
    return elements
        .slice(0, index)
        .reduce((width, element) => width + element.clientWidth, 0);
};

translateSlides = (deltaX = 0) => {
    slides.style.transform = `translateX(${-offset + deltaX}px)`;
};

shiftSlides = (newIndex = index) => {
    index = newIndex;
    offset = getOffset();
    translateSlides();
    if (index === 2) {
        imageSlider["src"] = image[1];
    } else {
        imageSlider["src"] = image[0];
    }
};

fakeInfinity = () => {
    switch (index) {
        case 0:
            return shiftSlides(elements.length - 2);
        case elements.length - 1:
            return shiftSlides(1);
        default:
    }
};

startDragging = (event) => {
    slides.style.transition = "none";
    dragStart = event.clientX;
    toggleDisabled(true);
};

doDragging = (event) => {
    if (dragStart === null) {
        return;
    }

    translateSlides(event.clientX - dragStart);
};

stopDragging = (event) => {
    if (dragStart === null) {
        return;
    }

    slides.style.transition = "";
    shiftSlides(index + (event.clientX < dragStart ? 1 : -1));
    dragStart = null;
};

elements.push(cloneElement(first, null));
elements.unshift(cloneElement(last, first));

[slides, ...elements].forEach(disableNativeDragging);

prevButton.addEventListener("click", () => shiftSlides(index - 1));
nextButton.addEventListener("click", () => shiftSlides(index + 1));

slides.addEventListener("mousedown", startDragging);
slides.addEventListener("mousemove", doDragging);
slides.addEventListener("mouseleave", stopDragging);
slides.addEventListener("mouseup", stopDragging);

slides.addEventListener("transitionstart", () => toggleDisabled(true));
slides.addEventListener("transitionend", () => toggleDisabled(false));
slides.addEventListener("transitionend", withTransitionSuspended(fakeInfinity));
window.addEventListener("resize", withTransitionSuspended(shiftSlides));
window.dispatchEvent(new Event("resize"));

// Validation
function Validator(formSelector) {
    var formRules = {};
    var _this = this;

    function getParentElement(element, selector) {
        while (element.parentElement) {
            if (element.parentElement.matches(selector)) {
                return element.parentElement;
            } else {
                element = element.parentElement;
            }
        }
    }

    var validatorRules = {
        required: (value) => {
            return value ? undefined : "Please input this field!";
        },
        email: (value) => {
            const regex =
                /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            return regex.test(value) ? undefined : "Invalid email";
        },
        password: (value) => {
            return value.length >= 8 ? undefined : "Password must have minimum of 8 characters";
        },
    };

    var formElement = document.querySelector(formSelector);
    if (formElement) {
        var inputs = formElement.querySelectorAll("[name][rules]");
        for (var input of inputs) {
            var rules = input.getAttribute("rules").split("|");
            for (var rule of rules) {
                if (Array.isArray(formRules[input.name])) {
                    formRules[input.name].push(validatorRules[rule]);
                } else {
                    formRules[input.name] = [validatorRules[rule]];
                }
            }
            input.onblur = handleValidate;
            input.oninput = handleClearError;
        }

        function handleValidate(event) {
            var rules = formRules[event.target.name];
            var errorMessage;
            for (var rule of rules) {
                errorMessage = rule(event.target.value);
                if (errorMessage) break;
            }

            if (errorMessage) {
                var formGroup = getParentElement(
                    event.target,
                    ".form__floating"
                );
                if (formGroup) {
                    formGroup.classList.add("invalid");
                    var formMessage = formGroup.querySelector(".form__error");
                    if (formMessage) {
                        formMessage.innerText = errorMessage;
                    }
                }
            }
            return !errorMessage;
        }

        function handleClearError(event) {
            var formGroup = getParentElement(event.target, ".form__floating");
            if (formGroup.classList.contains("invalid")) {
                formGroup.classList.remove("invalid");
                var formMessage = formGroup.querySelector(".form__error");
                if (formMessage) {
                    formMessage.innerText = "";
                }
            }
        }

        formElement.onsubmit = function (event) {
            event.preventDefault();

            var inputs = formElement.querySelectorAll("[name][rules]");
            var isValid = true;
            for (var input of inputs) {
                if (!handleValidate({ target: input })) {
                    isValid = false;
                }
            }

            if (isValid) {
                if (typeof _this.onSubmit === "function") {
                    _this.onSubmit();
                } else {
                    inputs.forEach((input) => {
                        console.log(`${input.name}: ${input.value}`);
                    });
                }
            }
        };
    }
}
